<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model {

    public function productdetail()
    {
        return $this->hasMany('App\ProductDetail', product_id);
    }

}
