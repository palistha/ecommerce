<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Price extends Model {
    public function productdetail()
    {
        return $this->hasMany('App\ProductDetail', price_id);
    }

}
