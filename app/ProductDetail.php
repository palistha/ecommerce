<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductDetail extends Model {


    public function brand()
    {
        return $this->belongsTo('App\Brand', brand_id);
    }
    public function color()
    {
        return $this->belongsTo('App\Color', color_id);
    }
    public function dimension()
    {
        return $this->belongsTo('App\Dimension', dimension_id);
    }
    public function models()
    {
        return $this->belongsTo('App\Models', models_id);
    }
    public function PowerBank()
    {
        return $this->belongsTo('App\PowerBank', powerbank_id);
    }
    public function Price()
    {
        return $this->belongsTo('App\Price', price_id);
    }
    public function product()
    {
        return $this->belongsTo('App\Product', product_id);
    }
    public function screenprotector()
    {
        return $this->belongsTo('App\ScreenProtector', screenprotector_id);
    }
}
