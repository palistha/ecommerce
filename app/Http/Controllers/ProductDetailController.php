<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\PowerBank;
use App\Brand;
use App\Color;
use App\Dimension;
use App\Models;
use App\Price;
use App\Product;
use App\ProductDetail;
use ValidateRequests;
use App\ScreenProtector;
use File;
use Image;
use Input;
use Illuminate\Validation\Validator;
class ProductDetailController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$product_details=ProductDetail::all();
//		dd($product_details);
    $product_details=ProductDetail::with('product')->orderBy('id', 'DESC')->get();
    dd($product_details);
	}


	/**if
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$brand=Brand::all(['id', 'name']);
		$color=Color::all(['id', 'name']);
		$dimension=Dimension::all(['id', 'name']);
		$models=Models::all(['id', 'name']);
		$powerbank=PowerBank::all(['id', 'name']);
		$price_type=Price::all(['id', 'type']);
		$product=Product::all(['id', 'name']);

		$screen_protector=ScreenProtector::all(['id', 'name']);
		return view('productdetails.create',compact('brand','color','dimension','models','powerbank','price_type','product','screen_protector'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
//	    dd($request->file('image'));
//	    dd($request);
        $this->validate($request,[
            'product_id' => 'required',

        ]);
        $product_details=new ProductDetail();

        $product_details->product_id=$request->product_id;
        if($request->brand_id!=""){
            $product_details->brand_id=$request->brand_id;
        }
        else{
//            echo "null";die;
            $product_details->brand_id=null;
        }
        if($request->price_id!=""){
            $product_details->price_id=$request->price_id;
        }
        else{
            $product_details->price_id=null;
        }

        if($request->price_id!=""){
            $product_details->color_id=$request->color_id;
        }
        else{
            $product_details->color_id=null;
        }

        if($request->dimension_id!=""){
            $product_details->dimension_id=$request->dimension_id;
        }
        else{
            $product_details->dimension_id=null;
        }
        if($request->powerbank_id!=""){
            $product_details->powerbank_id=$request->powerbank_id;
        }
        else{
            $product_details->powerbank_id=null;
        }
        if($request->screenprotector_id!=""){
            $product_details->screenprotector_id=$request->screenprotector_id;
        }
        else{
            $product_details->screenprotector_id=null;
        }

        if($request->models_id!=""){
            $product_details->models_id=$request->models_id;
        }
        else{
            $product_details->models_id=null;
        }


            $product_details->price=$request->price;

//        if($request->hasfile('image'))
//        {
//            $file = $request->file('image');
//            $extension = $file->getClientOriginalExtension(); // getting image extension
//            $filename =time().'.'.$extension;
//            $file->move('uploads/products/', $filename);
//        }


            $destinationPath = 'uploads/products';
            if (Input::file('image')) {
                $imagePath = str_random(6) . '_' . time() . "-" . $request->file('image')->getClientOriginalName();
                $product_details->image= $request->file('image')->move($destinationPath, $imagePath);

            }



            $result=$product_details->save();
        if($result){
            session()->flash('message', 'Product Detail Saved Succefully.');
            return redirect()->back();
        }
        else{
            session()->flash('message', 'Something Went Wrong! Please Try Again.');
            return redirect()->back()->withInput();
        }


    }
//    public function upload_image($image,$name) {
//        $image = \Image::make($image);
//        $path = 'uploads/products_resize/';
//        $image->resize(93,94);
//        // save resized
//        $image->save($path.$name);
//    }


    /**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
