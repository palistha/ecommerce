<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class PowerBank extends Model {

    public function productdetail()
    {
        return $this->hasMany('App\ProductDetail', powerbank_id);
    }

}
