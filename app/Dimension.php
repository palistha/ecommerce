<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Dimension extends Model
{

    //
    public function productdetail()
    {
        return $this->hasMany('App\ProductDetail', dimension_id);
    }
}