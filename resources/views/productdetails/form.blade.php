

<div class="row">
    <div class="col-md-4" >
        <div class="form-group">
            <label>Product</label>
            {{--<input type="text" class="form-control" name="name" >--}}
            <select id="state" name="product_id" class="form-control" >
                <option value="" class="default" selected>Select</option>
                @foreach ($product as $products)
                    <option value="{{ $products->id }}">{{ $products->name }}</option>
                @endforeach
            </select>
        @if ($errors->has('product_id'))
                <span class="text-danger"> {{$errors->first('product_id')}}</span>
            @endif
        </div>
    </div>

    <div class="col-md-4">
        <div class="form-group">
            <label>Image</label>
            <input type="file" class="form-control" name="image" >
            @if( isset($productdetails) && file_exists('uploads/products/'.$productdetails->image))

            @endif

        </div>
    </div>



</div>

<div class="row">
    <div class="col-md-4 " id="foo"  style="display:none">
        {{--<div class="form-group">--}}
        <div   class="form-group">
            <label>Brand</label>
            <select  name="brand_id" class="form-control" >
                <option value="" class="default" selected>Select</option>
                @foreach ($brand as $brands)
                    <option value="{{ $brands->id }}">{{ $brands->name }}</option>
                @endforeach
            </select>



        </div>
        {{--</div>--}}
    </div>
    <div class="col-md-4" id="price_type" style="display:none">
        <div class="form-group">
            <label>Price Type</label>
            <select  name="price_id" class="form-control" >
                <option value="" class="default" selected>Select</option>
                @foreach ($price_type as $price_types)
                    <option value="{{ $price_types->id }}">{{ $price_types->type }}</option>
                @endforeach
            </select>



        </div>
    </div>


</div>
    <div class="row" >
    <div class="col-md-4" >
        <div class="form-group">
            <label>Price </label>
            <input type="number" class="form-control" name="price" >


        </div>
    </div>
    <div class="col-md-4" id="color"  style="display:none">
        <div class="form-group">
            <label>Color</label>
            <select  name="color_id" class="form-control" >
                <option value="" class="default" selected>Select</option>
                @foreach ($color as $colors)
                    <option value="{{ $colors->id }}">{{ $colors->name }}</option>
                @endforeach
            </select>


        </div>
    </div>
    </div>
<div class="row">
    <div class="col-md-4" id="dimension"  style="display:none">
        <div class="form-group">
            <label>Dimension</label>
            <select  name="dimension_id" class="form-control" >
                <option value="" class="default" selected>Select</option>
                @foreach ($dimension as $dimensions)
                    <option value="{{ $dimensions->id }}">{{ $dimensions->name }}</option>
                @endforeach
            </select>


        </div>
    </div>
    <div class="col-md-4" id="powerbank"  style="display:none">
        <div class="form-group">
            <label>Power Bank Size</label>
            <select  name="powerbank_id" class="form-control" >
                <option value="" class="default" selected>Select</option>
                @foreach ($powerbank as $powerbanks)
                    <option value="{{ $powerbanks->id }}">{{ $powerbanks->name }}</option>
                @endforeach
            </select>


        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4" id="screenprotector"  style="display:none">
        <div class="form-group">
            <label>Screen Protector Type</label>
            <select  name="screenprotector_id" class="form-control" >
                <option value="" class="default" selected>Select</option>
                @foreach ($screen_protector as $screen_protectors)
                    <option value="{{ $screen_protectors->id }}">{{ $screen_protectors->name }}</option>
                @endforeach
            </select>


        </div>
    </div>
    <div class="col-md-4" id="models"  style="display:none">
        <div class="form-group">
            <label>Model</label>
            <select  name="models_id" class="form-control" >
                <option value="" class="default" selected>Select</option>
                @foreach ($models as $modelss)
                    <option value="{{ $modelss->id }}">{{ $modelss->name }}</option>
                @endforeach
            </select>


        </div>
    </div>


</div>



<script>
    $(document).ready(function (){
        $("#state").change(function() {
            // foo is the id of the other select box
            if ($(this).val() == 1) {
                $("#foo").show();
                $("#color").show();
                $("#dimension").show();
                $("#models").show();

                // $("#price").show();
                $("#price_type").show();
            }else{
                $("#foo").hide();
            }
            if($(this).val()==2){
                $('#screenprotector').show();
                $("#foo").hide();
                $("#color").hide();
                $("#dimension").hide();
                $("#models").hide();
                $('#powerbank').hide();
                // $("#price").show();
                $("#price_type").hide();
        }
        else{
                $('#screenprotector').hide();
            }
            if($(this).val()==3){
                $('#powerbank').show();
                $('#screenprotector').hide();
                $("#foo").hide();
                $("#color").hide();
                $("#dimension").hide();
                $("#models").hide();
                $("#price_type").hide();
                // $("#price").show();
            }else{
                $('#powerbank').hide();
            }
        });
    });
</script>