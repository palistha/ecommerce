@extends('app')
@section('content')
    <div class="col-md-12">
        {{--@if (count($errors) > 0)--}}
            {{--<div class="alert alert-danger">--}}
                {{--<strong>Whoops!</strong> There were some problems with your input.<br><br>--}}
              {{----}}
            {{--</div>--}}
        {{--@endif--}}
        @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $message }}</strong>
            </div>

        @endif
            {!! Form::open(['url' => 'productdetails', 'method'=>'POST', 'class' => 'common-form eq-half-form','files' => true,]) !!}

            @include('productdetails.form')
            <button type="submit" class="btn btn-info btn-fill">Submit
            </button>
            <div class="clearfix"></div>

            {!! Form::close() !!}
    </div>
@stop
