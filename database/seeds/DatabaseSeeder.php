<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();
   $userroles=
       array(
           array('role_type'=>'Admin'),
           array('role_type'=>'Staff'),


       );
   \App\Role::insert($userroles);
		// $this->call('UserTableSeeder');
        DB::table('users')->insert([
            'role_id'=>1,
            'name'=>'admin',
            'email'=>'admin@gmail.com',
            'password'=>bcrypt('password'),


        ]);
	}

}
